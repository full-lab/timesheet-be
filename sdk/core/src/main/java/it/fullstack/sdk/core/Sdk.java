package it.fullstack.sdk.core;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Import;

import lombok.extern.slf4j.Slf4j;

/**
 * SDK Main Class
 */
@Slf4j
@Import({
	ApplicationStartupLogger.class,
	SystemStartupLogger.class,
	OpenApiConfig.class,
	SdkCorsConfiguration.class
})
public class Sdk implements InitializingBean {
  
  @Override
  public void afterPropertiesSet() throws Exception {
    log.info(
        "\n----------------------------------------------------------\n\t" +
        "{} v.{}" + 
        "\n----------------------------------------------------------",
        this.getClass().getPackage().getImplementationTitle(),
        this.getClass().getPackage().getImplementationVersion()
    );
  }
}