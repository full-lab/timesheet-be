package it.fullstack.sdk.core.exception;

import lombok.Getter;

@Getter
public class Error {
    private String field;
    private String errorCode;
    private String defaultMessage;

    public Error(String field, String errorCode, String defaultMessage) {
        super();
        this.field = field;
        this.errorCode = errorCode;
        this.defaultMessage = defaultMessage;
    }

    public Error(String field, String errorCode, String defaultMessage, Object... args) {
        this(field, errorCode, String.format(defaultMessage, args));
    }
}
