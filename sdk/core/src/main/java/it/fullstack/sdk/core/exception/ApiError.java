package it.fullstack.sdk.core.exception;

import java.util.List;

import lombok.Getter;

@Getter
public class ApiError {
  private String code;
  private String message;
  private Error[] errors;

  public ApiError(String code, String message) {
    this.code = code;
    this.message = message;
  }

  public ApiError(String code, String message, Error... errors) {
    this(code, message);
    this.errors = errors;
  }

  public ApiError(String code, String message, List<Error> errors) {
    this(code, message, errors.toArray(new Error[0]));
  }

}
