package it.fullstack.sdk.core.exception;

public enum ErrorCode {
    FIELD_VALIDATION_ERROR("400_000001"),
    FORBIDDEN("400_100004"),
    ;

    public final String code;

    ErrorCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return code;
    }

}
