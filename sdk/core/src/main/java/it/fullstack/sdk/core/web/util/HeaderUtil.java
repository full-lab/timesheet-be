package it.fullstack.sdk.core.web.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;

/**
 * Utility class for HTTP headers creation.
 */
public final class HeaderUtil {

	private static final Logger log = LoggerFactory.getLogger(HeaderUtil.class);

	private HeaderUtil() {}

  /**
   * <p>createAlert.</p>
   *
   * @param applicationName a {@link java.lang.String} object.
   * @param message a {@link java.lang.String} object.
   * @param param a {@link java.lang.String} object.
   * @return a {@link org.springframework.http.HttpHeaders} object.
   */
  public static HttpHeaders createAlert(String applicationName, String message, String param) {
      HttpHeaders headers = new HttpHeaders();
      headers.add("X-" + applicationName + "-alert", message);
      try {
          headers.add("X-" + applicationName + "-params", URLEncoder.encode(param, StandardCharsets.UTF_8.toString()));
      } catch (UnsupportedEncodingException e) {
          // StandardCharsets are supported by every Java implementation so this exception will never happen
      }
      return headers;
  }

  /**
   * <p>createEntityCreationAlert.</p>
   *
   * @param applicationName a {@link java.lang.String} object.
   * @param entityName a {@link java.lang.String} object.
   * @param param a {@link java.lang.String} object.
   * @return a {@link org.springframework.http.HttpHeaders} object.
   */
	public static HttpHeaders createEntityCreationAlert(String applicationName, String entityName, String param) {
		return createAlert(applicationName, "A new " + entityName + " is created with identifier " + param, param);
	}

  /**
   * <p>createEntityUpdateAlert.</p>
   *
   * @param applicationName a {@link java.lang.String} object.
   * @param entityName a {@link java.lang.String} object.
   * @param param a {@link java.lang.String} object.
   * @return a {@link org.springframework.http.HttpHeaders} object.
   */
	public static HttpHeaders createEntityUpdateAlert(String applicationName, String entityName, String param) {
		return createAlert(applicationName, "A " + entityName + " is updated with identifier " + param, param);
	}

  /**
   * <p>createEntityDeletionAlert.</p>
   *
   * @param applicationName a {@link java.lang.String} object.
   * @param entityName a {@link java.lang.String} object.
   * @param param a {@link java.lang.String} object.
   * @return a {@link org.springframework.http.HttpHeaders} object.
   */
	public static HttpHeaders createEntityDeletionAlert(String applicationName, String entityName, String param) {
		return createAlert(applicationName, "A " + entityName + " is deleted with identifier " + param, param);
	}

  /**
   * <p>createFailureAlert.</p>
   *
   * @param applicationName a {@link java.lang.String} object.
   * @param entityName a {@link java.lang.String} object.
   * @param errorKey a {@link java.lang.String} object.
   * @param message a {@link java.lang.String} object.
   * @return a {@link org.springframework.http.HttpHeaders} object.
   */
  public static HttpHeaders createFailureAlert(String applicationName, String entityName, String errorKey, String message) {
      log.error("Entity processing failed, {}", message);

      HttpHeaders headers = new HttpHeaders();
      headers.add("X-" + applicationName + "-error", message);
      headers.add("X-" + applicationName + "-params", entityName);
      return headers;
  }
}
