package it.fullstack.sdk.core.exception.domain;

public interface DomainExceptionInterface {
    String getCode();
    String getMessage();
}
