package it.fullstack.sdk.core.exception;

import static it.fullstack.sdk.core.exception.ErrorCode.FIELD_VALIDATION_ERROR;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import it.fullstack.sdk.core.exception.domain.AbstractDomainException;
import it.fullstack.sdk.core.exception.domain.BadRequestException;
import it.fullstack.sdk.core.exception.domain.NotFoundException;
import jakarta.validation.ConstraintViolationException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractExceptionTranslator {
  
  private static final String GENERIC_ERROR_CODE = "400_000000";

  /**
   * Lista delle eccezioni per le quali non dovrà essere scritto il log di errore.
   * Indicare la lista (separata da virgola) nell'application.properties; es:
   * ```
   * exception-translator:
   *   log-exemptions: resid.sdk.core.exception.domain.NotFoundException, resid.sdk.core.exception.domain.BadRequestException
   * ```
   *
   * oppure multilinea
   *
   * ```
   * exception-translator:
   *   log-exemptions: >
   *     resid.sdk.core.exception.domain.NotFoundException,
   *     resid.sdk.core.exception.domain.BadRequestException
   * ```
   *
   * è possibile indicare una wildcard per referenziare un package parent, es:
   * ```
   * resid.sdk.core.exception.*
   * ```
   * per indicare tutte le classi il cui package dipende da `resid.sdk.core.exception`
   */
  @Value("${exception-translator.log-exemptions:}")
  private ArrayList<String> logExemptions = new ArrayList<>();
  
  @ExceptionHandler
  protected ResponseEntity<ApiError> on(MethodArgumentNotValidException ex) {
    var errors = ex.getBindingResult().getAllErrors()
        .stream()
        .map(FieldError.class::cast)
        .map(fieldError -> new Error(fieldError.getField(), fieldError.getCode(), fieldError.getDefaultMessage()))
        .toList();

      return processApiError(new ApiError(FIELD_VALIDATION_ERROR.code, ex.getMessage(), errors), HttpStatus.BAD_REQUEST, ex);
  }
  
  @ExceptionHandler
  protected ResponseEntity<ApiError> on(ConstraintViolationException ex) {
      var errors = ex.getConstraintViolations()
          .stream()
          .map(FieldError.class::cast)
          .map(fieldError -> new Error(fieldError.getField(), fieldError.getCode(), fieldError.getDefaultMessage()))
          .toList();      

      return processApiError(new ApiError(FIELD_VALIDATION_ERROR.code, ex.getMessage(), errors), HttpStatus.BAD_REQUEST, ex);
  }

  @ExceptionHandler
  protected ResponseEntity<ApiError> on(jakarta.persistence.EntityNotFoundException ex) {
      return processApiError(new ApiError(NotFoundException.CODE, ex.getMessage()), HttpStatus.NOT_FOUND, ex);
  }
  
  @ExceptionHandler
  protected ResponseEntity<ApiError> on(NotFoundException ex) {
      return processApiError(new ApiError(ex.getCode(), ex.getMessage()), HttpStatus.NOT_FOUND, ex);
  }

  @ExceptionHandler
  protected ResponseEntity<ApiError> on(BadRequestException ex) {
      return processApiError(new ApiError(ex.getCode(), ex.getMessage()), HttpStatus.BAD_REQUEST, ex);
  }
  
  @ExceptionHandler
  protected ResponseEntity<ApiError> on(AbstractDomainException ex) {
      return processApiError(new ApiError(ex.getCode(), ex.getMessage()), HttpStatus.BAD_REQUEST, ex);
  }
  
  /**
   * Handler di fallback per tutte le RuntimeException non gestite puntualmente
   */
  @ExceptionHandler
  protected ResponseEntity<ApiError> on(RuntimeException ex) {
      return processApiError(new ApiError(BadRequestException.CODE, ex.getMessage()), HttpStatus.BAD_REQUEST, ex);
  }
  
  /**
   * Handler di fallback per tutte le Throwable non gestite puntualmente
   */
  @ExceptionHandler
  protected ResponseEntity<ApiError> on(Throwable t, WebRequest request) {
      return processApiError(new ApiError(GENERIC_ERROR_CODE, t.getMessage()), HttpStatus.BAD_REQUEST, t);
  }
  
  protected ResponseEntity<ApiError> processApiError(ApiError dto, HttpStatus httpStatus, Throwable throwable) {
    if (!isLogExempted(throwable)) log.error("Exception: {}", throwable.getMessage(), throwable);
  
    return ResponseEntity
            .status(httpStatus)
            .body(dto);
  }

  private boolean isLogExempted(Throwable throwable) {
    String throwableClassName = throwable.getClass().getName();
    for (String logExemption: logExemptions) {
        if (null == logExemption || logExemption.isEmpty()) {
            // Pattern non applicabile
            continue;
        } else if (logExemption.endsWith("*") && throwableClassName.startsWith(logExemption.substring(0, logExemption.length() - 1))) {
            // Match wildcard
            return true;
        } else if(throwableClassName.equals(logExemption)) {
            // Match puntuale
            return true;
        }
    }
  
    return false;
  }
}
