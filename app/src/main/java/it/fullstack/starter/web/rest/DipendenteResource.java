package it.fullstack.starter.web.rest;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import it.fullstack.starter.domain.Dipendente;
import it.fullstack.starter.services.DipendenteService;
import it.fullstack.starter.services.dto.DipendenteCriteria;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/api/dipendente/v1")
public class DipendenteResource {

  private final DipendenteService service;
  private static final String ENTITY_NAME = "dipendente";

  @GetMapping("{id}")
  public ResponseEntity<Dipendente> findOne(@PathVariable String id) {
    log.debug("REST request to get {}", ENTITY_NAME);

    return ResponseEntity
           .of(service.findOne(id));
  }

  @GetMapping
  public ResponseEntity<Page<Dipendente>> findAll(DipendenteCriteria dipendente, Pageable page) {
    log.debug("REST request to get one {}", ENTITY_NAME);

    return ResponseEntity
           .ok(service.findAll(dipendente,page));
  }

  @PostMapping
  public ResponseEntity<Dipendente> create(@RequestBody Dipendente dto) throws URISyntaxException {
    log.debug("REST request to save {}", ENTITY_NAME);

    Dipendente saved = service.create(dto);

    return ResponseEntity
           .created(new URI("/api/dipendente/v1/" + saved.getCodiceFiscale()))
           .body(saved);
  }

  @PutMapping
  public ResponseEntity<Dipendente> update(@RequestBody Dipendente dto) {
    log.debug("REST request to update {}", ENTITY_NAME);

    Dipendente saved = service.update(dto);

    return ResponseEntity
           .ok(saved);
  }

  @DeleteMapping("{id}")
  public ResponseEntity<Void> delete(@PathVariable String id) {
    log.debug("REST request to delete {}", ENTITY_NAME);
      
    service.delete(id);
    
    return ResponseEntity
           .noContent()
           .build();
  }
}