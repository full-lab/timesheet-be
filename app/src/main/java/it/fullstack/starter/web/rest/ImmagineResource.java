package it.fullstack.starter.web.rest;

import java.io.IOException;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.fullstack.starter.domain.Immagine;
import it.fullstack.starter.services.ImmagineService;
import it.fullstack.starter.services.dto.UploadImmagineDTO;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/api/immagine/v1")
public class ImmagineResource {

  private final ImmagineService immagineService;
  private static final String ENTITY_NAME = "Immagine";

  @GetMapping("{id}")
  public ResponseEntity<Immagine> findImmagine(@PathVariable final long id) {
    log.debug("REST request to get one {}", ENTITY_NAME);
    
    return ResponseEntity
           .of(immagineService.findOne(id));
  }

  @PostMapping
  public ResponseEntity<Immagine> uploadImage(@Valid @ModelAttribute UploadImmagineDTO immagineDTO) throws IOException {
    log.debug("REST request to insert one {}", ENTITY_NAME);

    Immagine immagine = immagineService.uploadImage(immagineDTO);
    
    return ResponseEntity
           .ok(immagine);
  }

  @DeleteMapping("{id}")
  public ResponseEntity<Void> deleteImage(@PathVariable final long id) {
    log.debug("REST request to delete {}: {}", ENTITY_NAME, id);

    immagineService.deleteById(id);
    
    return ResponseEntity
           .noContent()
           .build();
  }
}