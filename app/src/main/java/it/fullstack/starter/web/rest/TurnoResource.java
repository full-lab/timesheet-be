package it.fullstack.starter.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.fullstack.sdk.core.exception.domain.BadRequestException;
import it.fullstack.sdk.core.exception.domain.NotFoundException;
import it.fullstack.starter.domain.Turno;
import it.fullstack.starter.repository.TurnoRepository;
import it.fullstack.starter.services.TurnoService;
import it.fullstack.starter.services.dto.CreateTurnoDTO;
import it.fullstack.starter.services.dto.TurnoCriteria;
import it.fullstack.starter.services.dto.UpdateTurnoDTO;
import it.fullstack.starter.web.errors.ErrorMessage;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/turno/v1")
@AllArgsConstructor
public class TurnoResource {
  private static final String ENTITY_NAME = "Turno";
  
  private final TurnoService service;
  private final TurnoRepository repository;
  
  @GetMapping("{id}")
  public ResponseEntity<Turno> findOne(@PathVariable Long id) {
    log.debug("REST request to get one {}", ENTITY_NAME);

    return ResponseEntity
           .of(repository.findById(id));
  }

  @GetMapping
  public ResponseEntity<List<Turno>> findAll(TurnoCriteria turno) {
    log.debug("REST request to get all {}", ENTITY_NAME);

    return ResponseEntity
           .ok(service.findAll(turno));
  }

  @PostMapping
  public ResponseEntity<Void> create(@Valid @RequestBody CreateTurnoDTO dto) throws URISyntaxException {
    log.debug("REST request to save {}: {}", ENTITY_NAME, dto);

    service.save(dto);

    return ResponseEntity
        .noContent()
        .build();
  }
  
  @PutMapping
  public ResponseEntity<Void> update(@RequestBody UpdateTurnoDTO dto) {
    log.debug("REST request to update {}", ENTITY_NAME);

    if (dto.getId() == null) throw new BadRequestException("Id cannot be null");
    if(!repository.existsById(dto.getId())) new NotFoundException(ErrorMessage.ENTITY_NOT_FOUND.message, dto.getId());

    service.update(dto);

    return ResponseEntity
        .noContent()
        .build();
  }

  @DeleteMapping("{id}")
  public ResponseEntity<Void> delete(@PathVariable Long id) {
    log.debug("REST request to delete {}: {}", ENTITY_NAME, id);

    service.delete(id);

    return ResponseEntity
           .noContent()
           .build();
  }
}