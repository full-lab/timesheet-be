package it.fullstack.starter.web.rest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import it.fullstack.starter.domain.Azienda;
import it.fullstack.starter.services.AziendaService;
import it.fullstack.starter.services.dto.AziendaCriteria;
import it.fullstack.starter.services.dto.CreateAziendaDTO;
import it.fullstack.starter.services.dto.UpdateAziendaDTO;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/api/azienda/v1")
public class AziendaResource {
    private static final String ENTITY_NAME = "Azienda";

    private final AziendaService aziendaService;

    @GetMapping("{id}")
    public ResponseEntity<Azienda> findOne(@PathVariable Long id) {
        log.debug("REST request to get one {}", ENTITY_NAME);
        return ResponseEntity.of(aziendaService.findOne(id));
    }

    @GetMapping
    public ResponseEntity<Page<Azienda>> findAll(AziendaCriteria criteria,Pageable page) {
        log.debug("REST request to get all: {}", ENTITY_NAME);
        return ResponseEntity.ok(aziendaService.findAll(criteria, page));
    }

    @PostMapping
    public ResponseEntity<Azienda> create(@Valid @RequestBody CreateAziendaDTO dto) throws URISyntaxException {
        log.debug("REST request to save {}: {}", ENTITY_NAME, dto);

        Azienda result = aziendaService.create(dto);

        return ResponseEntity
                .created(new URI("/api/azienda/v1/" + result.getId()))
                .body(result);
    }

    @PutMapping("{id}")
    public ResponseEntity<Azienda> update(@PathVariable Long id, @Valid @RequestBody UpdateAziendaDTO dto) {
        log.debug("REST request to update {}: {}", ENTITY_NAME, dto);

        return ResponseEntity.ok(aziendaService.update(id, dto));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.debug("REST request to delete {}: {}", ENTITY_NAME, id);
        
        aziendaService.delete(id);

        return ResponseEntity
            .noContent()
            .build();
    }
}
