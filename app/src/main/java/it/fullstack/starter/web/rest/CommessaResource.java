package it.fullstack.starter.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.fullstack.sdk.core.exception.domain.BadRequestException;
import it.fullstack.sdk.core.exception.domain.NotFoundException;
import it.fullstack.starter.config.SecurityUtils;
import it.fullstack.starter.domain.Commessa;
import it.fullstack.starter.repository.CommessaRepository;
import it.fullstack.starter.web.errors.ErrorMessage;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/api/commessa/v1")
public class CommessaResource {

  private static final String ENTITY_NAME = "Commessa";
  private final CommessaRepository repository;
  

  @GetMapping("{id}")
  public ResponseEntity<Commessa> findOne(@PathVariable Long id) {
    log.debug("REST request to get one {}", ENTITY_NAME);

    return ResponseEntity
           .of(repository.findById(id));
  }

  @GetMapping
  public ResponseEntity<List<Commessa>> findAll() {
    log.debug("REST request to get all: {}", ENTITY_NAME);
    
    return ResponseEntity
           .ok(repository.findAll());
  }
  
  @GetMapping("filtratePerUtente")
  public ResponseEntity<List<Commessa>> findFiltratePerUtente() {
    log.debug("REST request to get mine: {}", ENTITY_NAME);
    
    // Prelevo l'utente dal contesto Security
    String currentUser = SecurityUtils.getCurrentUserLogin().get();
//    var mieCommesse = dipendenteRepository
//      .findById(currentUser)
//      .map(d -> d.getCommesse())
//      .orElse(new ArrayList<>());
    var mieCommesse = repository.findByDipendentiCodiceFiscale(currentUser);
    
    return ResponseEntity
           .ok(mieCommesse);
  }

  @PostMapping
  public ResponseEntity<Commessa> create(@Valid @RequestBody Commessa dto) throws URISyntaxException {
    log.debug("REST request to save {}: {}", ENTITY_NAME, dto);

    if (dto.getId() != null) throw new BadRequestException(ErrorMessage.ID_NULL.message);
    
    Commessa result = repository.save(dto);
    
    return ResponseEntity
           .created(new URI("/api/commessa/v1/" + result.getId()))
           .body(result);
  }

  @PutMapping
  public ResponseEntity<Commessa> update(@Valid @RequestBody Commessa dto) {
    log.debug("REST request to update {}: {}", ENTITY_NAME, dto);
    
    if(dto.getId() == null) throw new BadRequestException("Id cannot be null");
    if(!repository.existsById(dto.getId())) throw new NotFoundException("%s non esistente: %s", ENTITY_NAME, dto.getId());
    if(repository.existsByIdAndDataChiusuraIsNotNull(dto.getId())) throw new BadRequestException("%s chiuso non può essere modificato: %s", ENTITY_NAME, dto.getId());

    return ResponseEntity
           .ok(repository.save(dto));

  }

  @DeleteMapping("{id}")
  public ResponseEntity<Void> delete(@PathVariable Long id) {
    log.debug("REST request to delete {}: {}", ENTITY_NAME, id);

    repository
        .findById(id)
        .map(d -> (Commessa) d.close())
        .map(repository::save)
        .orElseThrow(() -> new NotFoundException("%s non esistente: %s", ENTITY_NAME, id));
    

    return ResponseEntity
           .noContent()
           .build();
  }
}
