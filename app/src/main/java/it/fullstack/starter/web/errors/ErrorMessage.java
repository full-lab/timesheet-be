package it.fullstack.starter.web.errors;

public enum ErrorMessage {
    ID_NULL("Il campo ID non può essere valorizzato"),
    ID_NOT_NULL("Il campo ID non può essere null"),
    ID_MISMATCH("Il campo ID non corrisponde all'identificativo %s"),
    ID_CONFLICT("ID %s è già in uso"),
    ENTITY_NOT_FOUND("Entity con ID %s non trovata"),
    ENTITY_IS_DELETED("Entity con ID %s risulta cancellata"),
    ENTITY_LOCKED("Entity con ID %s è bloccata"),
    ENTITY_NOT_LINKED_TO("Entity con ID %s non risulta associata a %s"),
    CESPITE_NON_MODIFICABILE("Il cespite con ID %s non è modificabile"),
    CURRENT_USER_REQUIRED("Current-user required")
    ;

    public final String message;

    ErrorMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return message;
    }
}
