package it.fullstack.starter.web.rest;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.fullstack.starter.domain.Notifica;
import it.fullstack.starter.repository.NotificaRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/notifica/v1")
@AllArgsConstructor
public class NotificaResource {

	private final NotificaRepository repository;
	private static final String ENTITY_NAME = "Notifica";

	@GetMapping("{id}")
	public ResponseEntity<Notifica> findOne(@PathVariable Long id) {
		log.debug("REST request to get ONE {}", ENTITY_NAME);

		return ResponseEntity
		    .of(repository.findById(id));
	}

	@GetMapping
	public ResponseEntity<List<Notifica>> findAll() {
		log.debug("REST request to get ALL {}", ENTITY_NAME);

		return ResponseEntity
		    .ok(repository.findAll());
	}

	@DeleteMapping("{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		log.debug("REST request to delete {}: {}", ENTITY_NAME, id);

		repository.deleteById(id);

		return ResponseEntity
		    .noContent()
		    .build();
	}

}
