package it.fullstack.starter.web.rest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import it.fullstack.starter.domain.ReportMensile;
import it.fullstack.starter.repository.ReportMensileRepository;
import it.fullstack.starter.services.ReportMensileService;
import it.fullstack.starter.services.dto.InviaReportDTO;
import it.fullstack.starter.services.dto.ReportMensileCriteria;


@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/api/report/v1")
public class ReportMensileResource {

  private final ReportMensileService service;
  private final ReportMensileRepository repository;
  
  private static final String ENTITY_NAME = "Report Mensile";
  
  @GetMapping("{id}")
  public ResponseEntity<ReportMensile> findOne(@PathVariable Long id) {
    log.debug("REST request to get one {}", ENTITY_NAME);

    return ResponseEntity
           .of(repository.findById(id));
  }

  @GetMapping
  public ResponseEntity<Page<ReportMensile>> findAll(ReportMensileCriteria criteria, Pageable page) {
    log.debug("REST request to get all: {}", ENTITY_NAME);
    return ResponseEntity
           .ok(service.findAll(criteria, page));
  }
  
  @GetMapping("/mese")
  public ResponseEntity<ReportMensile> findOneByMese (ReportMensileCriteria criteria) {
    log.debug("REST request to get one: {}", ENTITY_NAME);

    return ResponseEntity
           .ok(service.findOneByCriteria(criteria));
  }

  @PostMapping("/{id}/richiedi-approvazione")
  public ResponseEntity<ReportMensile> richiediApprovazione(@PathVariable Long id, @RequestBody InviaReportDTO request){
    log.debug("REST request to richiediApprovazione {}: {}", ENTITY_NAME, request);
    
    ReportMensile result = service.richiediApprovazione(id, request);
    System.out.println(result.getStato());
    return ResponseEntity
        .ok(result);
  }
  
  @PostMapping("/{id}/approva")
  public ResponseEntity<ReportMensile> approva(@PathVariable Long id){
    log.debug("REST request to approva {}: {}", ENTITY_NAME, id);
    
    ReportMensile result = service.approva(id);
    
    return ResponseEntity
        .ok(result);
  }
  
  @PostMapping("/{id}/rifiuta")
  public ResponseEntity<ReportMensile> rifiuta(@PathVariable Long id, @RequestBody InviaReportDTO request){
    log.debug("REST request to rifiuta {}: {}", ENTITY_NAME, request);
    
    ReportMensile result = service.rifiuta(id, request);
    
    return ResponseEntity
        .ok(result);
  }
  
}
