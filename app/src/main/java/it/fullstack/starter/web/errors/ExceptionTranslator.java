package it.fullstack.starter.web.errors;

import org.springframework.web.bind.annotation.ControllerAdvice;

import it.fullstack.sdk.core.exception.AbstractExceptionTranslator;

@ControllerAdvice
public class ExceptionTranslator extends AbstractExceptionTranslator {

}
