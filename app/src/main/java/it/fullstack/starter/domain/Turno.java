package it.fullstack.starter.domain;

import java.time.Duration;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import it.fullstack.starter.domain.validator.SameDay;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@Entity
@SameDay
@EqualsAndHashCode(of = { "start", "end" })
public class Turno {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotNull
  @Column(name = "start_date")
  private LocalDateTime start;

  @NotNull
  @Column(name = "end_date")
  private LocalDateTime end;
  
  private String title;

  @ManyToOne
  private Commessa commessa;

  @ManyToOne
  @JsonIgnoreProperties(value = {"commesse","reports","mansione"})
  private Dipendente dipendente;

  private String numProtocollo;

  // FIXME Alessio Prova a verificare se funziona tutto anche senza la navigazione bi-laterale
  @ManyToOne
  @ToString.Exclude
  @JsonIgnoreProperties(value = { "dipendente", "turni" })
  private ReportMensile report;

  /*
   * Calcola la differenza tra le due istanze di LocalDateTime
   */
  public long getOre() {
    return Duration.between(start, end).toHours();
  }
  
  public Integer getGiorno() {
    return start.getDayOfMonth();
  }

  public Integer getMese() {
    return start.getMonthValue();
  }

  public Integer getAnno() {
    return start.getYear();
  }
  
  public String getTitle() {
    return this.title;
  }

  public Turno id(Long id) {
    this.id = id;
    return this;
  }

  public Turno start(LocalDateTime start) {
    this.start = start;
    return this;
  }

  public Turno end(LocalDateTime end) {
    this.end = end;
    return this;
  }

  public Turno commessa(Commessa commessa) {
    this.commessa = commessa;
    return this;
  }

  public Turno numProtocollo(String numProtocollo) {
    this.numProtocollo = numProtocollo;
    return this;
  }

  public Turno dipendente(Dipendente dipendente) {
    this.dipendente = dipendente;
    return this;
  }

  public Turno report(ReportMensile report) {
    this.report = report;
    return this;
  }
  
  public Turno title(String title) {
    this.title = title;
    return this;
  }

}