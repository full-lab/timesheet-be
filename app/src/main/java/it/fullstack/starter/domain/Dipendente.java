package it.fullstack.starter.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import it.fullstack.starter.domain.tipologiche.Mansione;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@EqualsAndHashCode(of = "codiceFiscale", callSuper = true)
public class Dipendente extends AbstractAuditingEntity {

  private static final long serialVersionUID = 1L;

  @Id
  @NotNull
  private String codiceFiscale;
  
  @NotNull
  private String nome;
  
  @NotNull
  private String cognome;
  
  private String email;
  private LocalDate dataNascita;

  //@NotNull
  @ManyToOne
  private Mansione mansione;

  @JsonIgnoreProperties("azienda")
  @ManyToMany(fetch = FetchType.EAGER)
  private List<Commessa> commesse = new ArrayList<>();

  @JsonIgnore
  @OneToMany(mappedBy = "dipendente")
  private List<ReportMensile> reports = new ArrayList<>();
  
  public Dipendente codiceFiscale(String codiceFiscale) {
    this.codiceFiscale = codiceFiscale;
    return this;
  }

  public Dipendente nome(String nome) {
    this.nome = nome;
    return this;
  }

  public Dipendente cognome(String cognome) {
    this.cognome = cognome;
    return this;
  }

  public Dipendente email(String email) {
    this.email = email;
    return this;
  }

  public Dipendente dataNascita(LocalDate dataNascita) {
    this.dataNascita = dataNascita;
    return this;
  }

  public Dipendente mansione(Mansione mansione) {
    this.mansione = mansione;
    return this;
  }

  public Dipendente commesse(List<Commessa> commesse) {
    this.commesse = commesse;
    return this;
  }

  public Dipendente reports(List<ReportMensile> reports) {
    this.reports = reports;
    return this;
  }

}