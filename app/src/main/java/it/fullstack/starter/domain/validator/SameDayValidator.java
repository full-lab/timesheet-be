package it.fullstack.starter.domain.validator;

import java.time.LocalDateTime;

import it.fullstack.starter.domain.Turno;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class SameDayValidator implements ConstraintValidator<SameDay, Turno> {

  @Override
  public boolean isValid(Turno truno, ConstraintValidatorContext context) {
    if (truno == null)
      return true; // Null values are considered valid

    LocalDateTime start = truno.getStart();
    LocalDateTime end = truno.getEnd();

    return start != null && end != null && start.toLocalDate().isEqual(end.toLocalDate());
  }
}
