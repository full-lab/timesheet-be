package it.fullstack.starter.domain.enumeration;

public enum StatoReport {
  IN_LAVORAZIONE,
  RICHIESTA_VALIDAZIONE,
  APPROVATO,
  RIFIUTATO,
}