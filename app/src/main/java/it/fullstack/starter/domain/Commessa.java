package it.fullstack.starter.domain;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonIncludeProperties;

import it.fullstack.starter.domain.enumeration.TipoCommessa;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@EqualsAndHashCode(of = "id", callSuper = false)
public class Commessa extends AbstractAuditingEntity {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotNull
  private String nome;

  @NotNull
  private String color;
  
  @JsonIncludeProperties({"id", "nome", "partitaIva", "closed"})
  @ManyToOne
  private Azienda azienda;

  @NotNull
  @Enumerated(EnumType.STRING)
  private TipoCommessa tipoCommessa;
  
  @JsonIncludeProperties({"nome", "cognome", "codiceFiscale", "dataNascita", "email" })
  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(
      name = "DIPENDENTE_COMMESSE", // nome della tabella di join
      joinColumns = @JoinColumn(name = "COMMESSE_ID"), // colonna nella tabella commessa
      inverseJoinColumns = @JoinColumn(name = "DIPENDENTE_CODICE_FISCALE") // colonna nella tabella dipendente
  )
  private List<Dipendente> dipendenti = new ArrayList<>();

  public Commessa id(Long id) {
    this.id = id;
    return this;
  }

  public Commessa nome(String nome) {
    this.nome = nome;
    return this;
  }

  public Commessa azienda(Azienda azienda) {
    this.azienda = azienda;
    return this;
  }

  public Commessa tipoCommessa(TipoCommessa tipoCommessa) {
    this.tipoCommessa = tipoCommessa;
    return this;
  }

}