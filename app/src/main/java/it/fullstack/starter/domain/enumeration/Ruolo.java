package it.fullstack.starter.domain.enumeration;

public enum Ruolo {
  // predisposizione futura per nuovi ruoli
  DIPENDENTE, ADMIN

}
