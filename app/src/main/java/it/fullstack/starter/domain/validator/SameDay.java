package it.fullstack.starter.domain.validator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

@Target({ ElementType.TYPE, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = SameDayValidator.class)
public @interface SameDay {
  String message() default "Le date devono appartenere allo stesso giorno";
  Class<?>[] groups() default {};
  Class<? extends Payload>[] payload() default {};
}

