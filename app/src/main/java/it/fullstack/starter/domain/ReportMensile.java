package it.fullstack.starter.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import it.fullstack.sdk.core.exception.domain.BadRequestException;
import it.fullstack.starter.domain.enumeration.StatoReport;
import it.fullstack.starter.repository.TurnoRepository;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.ToString;

@Data
@Entity
public class ReportMensile implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotNull
  private Integer anno;

  @NotNull
  private Integer mese;

  @ManyToOne
  @JsonIgnoreProperties(value = { "commesse", "reports", "mansione" })
  private Dipendente dipendente;

  @Enumerated(EnumType.STRING)
  private StatoReport stato = StatoReport.IN_LAVORAZIONE;

  private String notaInvio;
  private String notaRifiuto;

  @ToString.Exclude
  @JsonIgnoreProperties(value = "report")
  @OneToMany(mappedBy = "report", fetch = FetchType.EAGER)
  private Set<Turno> turni = new HashSet<>();

  public ReportMensile id(Long id) {
    this.id = id;
    return this;
  }

  public ReportMensile mese(Integer mese) {
    this.mese = mese;
    return this;
  }

  public ReportMensile anno(Integer anno) {
    this.anno = anno;
    return this;
  }

  public ReportMensile dipendente(Dipendente dipendente) {
    this.dipendente = dipendente;
    return this;
  }

  public ReportMensile statoReport(StatoReport statoReport) {
    this.stato = statoReport;
    return this;
  }

  public ReportMensile notaInvio(String notaInvio) {
    this.notaInvio = notaInvio;
    return this;
  }

  public ReportMensile notaRifiuto(String notaRifiuto) {
    this.notaRifiuto = notaRifiuto;
    return this;
  }

  public ReportMensile addTurno(Turno turno) {
    if (! (StatoReport.IN_LAVORAZIONE.equals(stato) || StatoReport.RIFIUTATO.equals(stato)) )
      throw new BadRequestException("Report non modificabile");

    this.turni.add(turno);
    turno.setReport(this);

    return this;
  }
  
  public ReportMensile removeTurno(Turno turno) {
    if (! (StatoReport.IN_LAVORAZIONE.equals(stato) || StatoReport.RIFIUTATO.equals(stato)) )
      throw new BadRequestException("Report non modificabile");

    this.turni.remove(turno);    
    turno.setReport(null);

    return this;
  }

  public ReportMensile richiediApprovazione(String nota) {
    if (!StatoReport.IN_LAVORAZIONE.equals(stato)) throw new BadRequestException("Impossibile richiedere la approvazione per il Report");

    this.notaInvio = nota;
    this.stato = StatoReport.RICHIESTA_VALIDAZIONE;
    return this;
  }

  public ReportMensile approva() {
    if (!StatoReport.RICHIESTA_VALIDAZIONE.equals(stato)) throw new BadRequestException("Impossibile approvare il Report");

    this.stato = StatoReport.APPROVATO;
    return this;
  }

  public ReportMensile rifiuta(String nota) {
    if (!StatoReport.RICHIESTA_VALIDAZIONE.equals(stato)) throw new BadRequestException("Impossibile rifiutare il Report");

    this.notaRifiuto = nota;
    this.stato = StatoReport.RIFIUTATO;
    return this;
  }

}
