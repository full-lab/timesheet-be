package it.fullstack.starter.domain;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@EqualsAndHashCode(of = "id", callSuper = true)
public class Azienda extends AbstractAuditingEntity {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotNull
  @Column(unique = true)
  private String nome;

  @NotNull
  @Column(unique = true)
  @Pattern(regexp = "^[0-9]{11}$", message = "La partita IVA deve essere composta da 11 cifre")
  private String partitaIva;

  @JsonIgnore
  @OneToOne(cascade = CascadeType.REMOVE)
  private Immagine logo;

  @JsonIgnoreProperties(value = {"azienda"}, allowSetters = true)
  @OneToMany(mappedBy = "azienda", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  private List<Commessa> commesse = new ArrayList<>();
  
  public void setNome(String nome) {
   this.nome = Character.toUpperCase(nome.charAt(0)) + nome.substring(1);

  }

  public Azienda id(Long id) {
    this.id = id;
    return this;
  }

  public Azienda nome(String nome) {
    setNome(nome);
    return this;
  }

  public Azienda partitaIva(String partitaIva) {
    this.partitaIva = partitaIva;
    return this;
  }

  public Azienda logo(Immagine logo) {
    this.logo = logo;
    return this;
  }

  public Azienda commesse(List<Commessa> commesse) {
    this.commesse = commesse;
    return this;
  }

}