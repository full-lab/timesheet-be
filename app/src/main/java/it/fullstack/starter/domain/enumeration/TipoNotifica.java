package it.fullstack.starter.domain.enumeration;

public enum TipoNotifica {

  REPORT_APPROVATO("Il tuo report è stato approvato!"), REPORT_RIFIUTATO("Il tuo report è stato rifiutato!"),
  SOLLECITA_INVIO_REPORT_3_GIORNI("Mancano 3 giorni allo scadere del mese, invia il tuo report!"),
  SOLLECITA_INVIO_REPORT_ULTIMO_GIORNO("Oggi è l'ultimo giorno per inviare il tuo report, affrettati!"),
  SOLLECITO_MANUALE("L'admin ti ha sollecitato ad inviare il report mensile"),
  DATI_MODIFICATI("Dati personali modificati"), REGISTRATO_MALATTIA("Periodo di malattia registrato"), GENERICA("");

  public String descrizione;

  TipoNotifica(String descrizione) {
    this.descrizione = descrizione;
  }

  @Override
  public String toString() {
    return descrizione;
  }
}
