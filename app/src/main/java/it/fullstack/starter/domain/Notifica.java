package it.fullstack.starter.domain;

import java.time.LocalDateTime;

import it.fullstack.starter.domain.enumeration.TipoNotifica;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.Data;

@Data
@Entity
// FIXME X Pietro Sistemare
public class Notifica {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private LocalDateTime dataEmissione;
  private String messaggio;
  private Boolean visualizzata;

//  @ManyToOne
//  private Utente emittente;
//
//  @ManyToOne
//  private Utente ricevente;

  @Enumerated(EnumType.STRING)
  private TipoNotifica tipoNotifica;

  @ManyToOne
  private ReportMensile report;

}