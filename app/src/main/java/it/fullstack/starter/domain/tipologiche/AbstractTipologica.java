package it.fullstack.starter.domain.tipologiche;

import it.fullstack.starter.domain.AbstractAuditingEntity;
import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@MappedSuperclass
@EqualsAndHashCode(of = "id", callSuper = false)
public abstract class AbstractTipologica extends AbstractAuditingEntity {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  protected Long id;

  @NotNull
  @Column(nullable = false, unique = true)
  private String descrizione;

  public AbstractTipologica id(Long id) {
    this.id = id;
    return this;
  }

  public AbstractTipologica descrizione(String descrizione) {
    this.descrizione = descrizione;
    return this;
  }

}
