package it.fullstack.starter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import it.fullstack.starter.domain.Azienda;

@Repository
public interface AziendaRepository extends JpaRepository<Azienda, Long>, JpaSpecificationExecutor<Azienda> {
  boolean existsByIdAndDataChiusuraIsNotNull(Long id);

}
