package it.fullstack.starter.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.fullstack.starter.domain.Commessa;
import it.fullstack.starter.domain.Dipendente;

@Repository
public interface CommessaRepository extends JpaRepository<Commessa, Long> {

  boolean existsByIdAndDataChiusuraIsNotNull(Long id);
  List<Commessa> findByDipendentiContains(Dipendente dipendente);
  List<Commessa> findByDipendentiCodiceFiscale(String dipendenteId);
}
