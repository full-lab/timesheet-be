package it.fullstack.starter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import it.fullstack.starter.domain.Turno;

@Repository
public interface TurnoRepository extends JpaRepository<Turno, Long>, JpaSpecificationExecutor<Turno>{
}
