package it.fullstack.starter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import it.fullstack.starter.domain.Dipendente;


@Repository
public interface DipendenteRepository extends JpaRepository<Dipendente, String>, JpaSpecificationExecutor<Dipendente> {

  boolean existsByCodiceFiscaleAndDataChiusuraIsNotNull(String codiceFiscale);
}
