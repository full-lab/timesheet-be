package it.fullstack.starter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.fullstack.starter.domain.Notifica;

@Repository
public interface NotificaRepository extends JpaRepository<Notifica, Long> {
}
