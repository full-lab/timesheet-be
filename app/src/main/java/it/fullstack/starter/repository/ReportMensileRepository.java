package it.fullstack.starter.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import it.fullstack.starter.domain.Dipendente;
import it.fullstack.starter.domain.ReportMensile;

@Repository
public interface ReportMensileRepository extends JpaRepository<ReportMensile, Long>, JpaSpecificationExecutor<ReportMensile>{
	 
	Optional<ReportMensile> findByAnnoAndMeseAndDipendente(Integer anno, Integer mese, Dipendente dipendente);
}