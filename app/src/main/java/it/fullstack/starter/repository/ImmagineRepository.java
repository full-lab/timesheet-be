package it.fullstack.starter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.fullstack.starter.domain.Immagine;

@Repository
public interface ImmagineRepository extends JpaRepository<Immagine, Long> {

}
