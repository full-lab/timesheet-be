package it.fullstack.starter.services.dto;

import java.time.LocalDateTime;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class CreateTurnoDTO {
  
  @NotNull
  private LocalDateTime start;

  @NotNull
  private LocalDateTime end;
  
  private String title;

  @NotNull
  private Long commessaId;
  
  private String numeroProtocollo;

  private boolean allDay;
  private boolean includeWeekend;
  
}