package it.fullstack.starter.services.dto;

import org.springframework.web.multipart.MultipartFile;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class UploadImmagineDTO {

  @NotNull
  private MultipartFile immagine;
  
  private Long aziendaId;
  
}