package it.fullstack.starter.services.dto;

import java.util.ArrayList;
import java.util.List;

import it.fullstack.starter.domain.Commessa;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class UpdateAziendaDTO extends CreateAziendaDTO{
  
  private List<Commessa> commesse = new ArrayList<>();
  
}
