package it.fullstack.starter.services.dto;

import it.fullstack.starter.domain.Immagine;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class CreateAziendaDTO {
  
  @NotNull
  private String nome;
  
  @NotNull
  private String partitaIva;
  
  private Immagine logo;

}
