package it.fullstack.starter.services;

import it.fullstack.sdk.core.exception.domain.NotFoundException;
import it.fullstack.sdk.core.service.QueryService;
import it.fullstack.sdk.core.service.filter.StringFilter;
import it.fullstack.starter.config.Constants;
import it.fullstack.starter.config.SecurityUtils;
import it.fullstack.starter.domain.Dipendente_;
import it.fullstack.starter.domain.ReportMensile;
import it.fullstack.starter.domain.ReportMensile_;
import it.fullstack.starter.repository.ReportMensileRepository;
import it.fullstack.starter.services.dto.InviaReportDTO;
import it.fullstack.starter.services.dto.ReportMensileCriteria;
import it.fullstack.starter.web.errors.ErrorMessage;
import jakarta.persistence.criteria.JoinType;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@AllArgsConstructor
public class ReportMensileService extends QueryService<ReportMensile> {

  private final ReportMensileRepository repository;

  
  @Transactional(readOnly = true)
  public Page<ReportMensile> findAll(ReportMensileCriteria criteria, Pageable page) {
    return repository
        .findAll(createSpecification(criteria), page);
  }
   
  @Transactional(readOnly = true)
  public Optional<ReportMensile> findOne(Long id) {
	  log.debug("Request to get Report:{}", id);
	  
	  return repository.findById(id);
  }
  
  @Transactional(readOnly = true)
  public List<ReportMensile> findAll() {
	  log.debug("Request to get all Report:{}");
	  
      return repository.findAll();
  }
  
  @Transactional(readOnly = true)
  public ReportMensile findOneByCriteria(ReportMensileCriteria criteria) {
    log.debug("Request to get all Report:{}");
    
      return repository
          .findOne(createSpecification(criteria))
          .orElseThrow(() -> new NotFoundException(ErrorMessage.ENTITY_NOT_FOUND.message, criteria));
  }
  
  @Transactional
  public ReportMensile richiediApprovazione(Long reportId, InviaReportDTO request) {
    return repository
      .findById(reportId)
      .map(r -> r.richiediApprovazione(request.getNota()))
      .map(repository::save)
      .orElseThrow(() -> new NotFoundException(ErrorMessage.ENTITY_NOT_FOUND.message, reportId));
    
  }
  
  @Transactional
  public ReportMensile approva(Long reportId) {
    return repository
      .findById(reportId)
      .map(r -> r.approva())
      .map(repository::save)
      .orElseThrow(() -> new NotFoundException(ErrorMessage.ENTITY_NOT_FOUND.message, reportId));
  }
  
  @Transactional
  public ReportMensile rifiuta(Long reportId, InviaReportDTO request) {
    return repository
      .findById(reportId)
      .map(r -> r.rifiuta(request.getNota()))
      .map(repository::save)
      .orElseThrow(() -> new NotFoundException(ErrorMessage.ENTITY_NOT_FOUND.message, reportId));
  }
  
  private Specification<ReportMensile> createSpecification(ReportMensileCriteria criteria) {
    Specification<ReportMensile> specification = Specification.where(null);

    if (criteria != null) {
      
      if (criteria.getAnno() != null) {
        
        specification = specification.and(
            equal(
                criteria.getAnno(), ReportMensile_.anno));
    }
      
      if(criteria.getMese() != null) {
        
        specification = specification.and(
            equal(
                criteria.getMese(), ReportMensile_.mese)
            );
      }
      
      if(criteria.getStato() != null) {
        
        specification = specification.and(
            equal(
                criteria.getStato(),
                ReportMensile_.stato
            ));
      }
      
      if(criteria.getQ() != null && !criteria.getQ().isBlank()) {
        
        Specification<ReportMensile> nome = startsUpperSpecification(root -> 
                                                    root.join(ReportMensile_.dipendente, JoinType.INNER)
                                                        .get(Dipendente_.nome), criteria.getQ());
        
        Specification<ReportMensile> cognome = startsUpperSpecification(root -> 
                                                    root.join(ReportMensile_.dipendente, JoinType.INNER)
                                                        .get(Dipendente_.cognome), criteria.getQ());
        
        specification = specification.and(nome.or(cognome));
      }
      
      if (!SecurityUtils.isCurrentUserInRole(Constants.RESPONSABILE)) {
        
        // Prelevo l'Utente
        String currentUserName = SecurityUtils.getCurrentUserLogin().get();  
        
        StringFilter user = new StringFilter();
        user.setEquals(currentUserName);
        
        specification = specification.and(buildSpecification(user, 
            root -> root.join(ReportMensile_.dipendente, JoinType.LEFT).get(Dipendente_.codiceFiscale)));
      }
      
    }
    return specification;
  }
}