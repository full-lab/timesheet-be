package it.fullstack.starter.services.mapper;

import it.fullstack.starter.domain.Azienda;
import it.fullstack.starter.services.dto.CreateAziendaDTO;
import it.fullstack.starter.services.dto.UpdateAziendaDTO;

public interface AziendaMapper {
  
  public static Azienda toEntityUpdate(UpdateAziendaDTO dto) {
    Azienda azienda = new Azienda();
    azienda.setNome(dto.getNome());
    azienda.setPartitaIva(dto.getPartitaIva());
    azienda.setLogo(dto.getLogo());

    return azienda;
  }
  
  public static Azienda toEntityCreate(CreateAziendaDTO dto) {
    Azienda azienda = new Azienda();
    azienda.setNome(dto.getNome());
    azienda.setPartitaIva(dto.getPartitaIva());
    azienda.setLogo(dto.getLogo());

    return azienda;
  }

}
