package it.fullstack.starter.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fullstack.sdk.core.exception.domain.BadRequestException;
import it.fullstack.sdk.core.exception.domain.NotFoundException;
import it.fullstack.sdk.core.service.QueryService;
import it.fullstack.starter.domain.Azienda;
import it.fullstack.starter.domain.Azienda_;
import it.fullstack.starter.domain.Commessa;
import it.fullstack.starter.repository.AziendaRepository;
import it.fullstack.starter.repository.CommessaRepository;
import it.fullstack.starter.services.dto.AziendaCriteria;
import it.fullstack.starter.services.dto.CreateAziendaDTO;
import it.fullstack.starter.services.dto.UpdateAziendaDTO;
import it.fullstack.starter.services.mapper.AziendaMapper;
import it.fullstack.starter.web.errors.ErrorMessage;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@AllArgsConstructor
public class AziendaService extends QueryService<Azienda> {
  
    private static final String ENTITY_NAME = "Azienda";
    
    private final AziendaRepository aziendaRepository;
    private final CommessaRepository commessaRepository;
  
    @Transactional(readOnly = true)
    public Optional<Azienda> findOne(Long id) {
      log.debug("Request to get Azienda:{}", id);
      
      return aziendaRepository.findById(id);
    }
  
    @Transactional(readOnly = true)
    public Page<Azienda> findAll(AziendaCriteria criteria, Pageable page) {
      log.debug("Request to get all Aziende:{}");
      
      return aziendaRepository.findAll(createSpecification(criteria), page);
    }
  
    @Transactional
    public Azienda create(CreateAziendaDTO dto) {
      log.debug("Request to create Aziende:{}");
      
      var aziendaUpdate = AziendaMapper.toEntityCreate(dto);

      return aziendaRepository.save(aziendaUpdate);
    }
  
    @Transactional
    public Azienda update(Long id, UpdateAziendaDTO dto){
      log.debug("Request to update Aziende:{}");
      
      var aziendaUpdate = AziendaMapper.toEntityUpdate(dto);
      
      if (id == null) throw new BadRequestException(ErrorMessage.ID_NOT_NULL.message);
      if(!aziendaRepository.existsById(id)) throw new NotFoundException("%s non esistente: %s", ENTITY_NAME, id);
      if (aziendaRepository.existsByIdAndDataChiusuraIsNotNull(id)) throw new BadRequestException(ErrorMessage.ENTITY_LOCKED.message, id);
      
      aziendaUpdate.setId(id);
      
      if(!dto.getCommesse().isEmpty()) {
               
        dto.getCommesse().forEach(commessa -> {
          Commessa commessaFromDB = commessaRepository
                  .findById(commessa.getId())
                  .orElseThrow(() -> new NotFoundException(ErrorMessage.ENTITY_NOT_FOUND.message, commessa.getId()));

          if (!commessaFromDB.getAzienda().getId().equals(commessa.getAzienda().getId())) {
              throw new NotFoundException(ErrorMessage.ENTITY_NOT_LINKED_TO.message, id ,commessa.getId());
          }
        });
                
        aziendaUpdate.setCommesse(dto.getCommesse());
      }
      
      return aziendaRepository.save(aziendaUpdate);
    }
  
    public void delete(Long id) {
      Azienda azienda = aziendaRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("%s non esistente: %s", ENTITY_NAME, id));
  
      aziendaRepository.save((Azienda) azienda.close());
    }    
    
    private Specification<Azienda> createSpecification(AziendaCriteria criteria){
      Specification<Azienda> specification = Specification.where(null);
      if(criteria != null) {
        
        if (criteria.getNome() != null) {
          specification = startsUpperSpecification(root -> 
            root.get(Azienda_.nome), "%" + criteria.getNome() + "%"
             );
         }
      }
      return specification;
    }
}
