package it.fullstack.starter.services;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fullstack.sdk.core.exception.domain.BadRequestException;
import it.fullstack.sdk.core.exception.domain.NotFoundException;
import it.fullstack.sdk.core.service.QueryService;
import it.fullstack.sdk.core.service.filter.StringFilter;
import it.fullstack.starter.config.Constants;
import it.fullstack.starter.config.SecurityUtils;
import it.fullstack.starter.domain.Commessa;
import it.fullstack.starter.domain.Dipendente;
import it.fullstack.starter.domain.Dipendente_;
import it.fullstack.starter.domain.ReportMensile;
import it.fullstack.starter.domain.Turno;
import it.fullstack.starter.domain.Turno_;
import it.fullstack.starter.domain.enumeration.StatoReport;
import it.fullstack.starter.repository.CommessaRepository;
import it.fullstack.starter.repository.DipendenteRepository;
import it.fullstack.starter.repository.ReportMensileRepository;
import it.fullstack.starter.repository.TurnoRepository;
import it.fullstack.starter.services.dto.CreateTurnoDTO;
import it.fullstack.starter.services.dto.TurnoCriteria;
import it.fullstack.starter.services.dto.UpdateTurnoDTO;
import jakarta.persistence.criteria.JoinType;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class TurnoService extends QueryService<Turno>{

  private final TurnoRepository turnoRepository;
  private final ReportMensileRepository reportMensileRepository;
  private final DipendenteRepository dipendenteRepository;
  private final CommessaRepository commessaRepository;


  public List<Turno> findAll(TurnoCriteria criteria){

    return turnoRepository.findAll(createSpecification(criteria));
  }

  /**
   * Questo metodo accetta dal Frontend il DTO, lo converte in uno o più Turni a sedonda dell'intervallo start-end
   * @param dto
   * @return
   */
  @Transactional
  public void save(CreateTurnoDTO dto) {

    // Prelevo l'Utente
    String currentUserName = SecurityUtils.getCurrentUserLogin().get();

    // Prelevo il Dipendente
    Dipendente dipendente = dipendenteRepository.getReferenceById(currentUserName);

    //divido in mattina e pomeriggio
    final LocalTime startBreak = LocalTime.of(13, 0);
    final LocalTime endBreak = LocalTime.of(14, 0);
    
    // Iterazione su ogni giorno
    final LocalTime starTime = dto.isAllDay() ? LocalTime.of(9, 0) : dto.getStart().toLocalTime();
    final LocalTime endTime = dto.isAllDay() ? LocalTime.of(18, 0) : dto.getEnd().toLocalTime();

    final LocalDate startDay = dto.getStart().toLocalDate();
    final LocalDate endDay = dto.getEnd().toLocalDate();

    LocalDate currentDate = startDay;
    
    if(overlaps(dto, dipendente)) throw new BadRequestException("Ci sono già turni in questo orario");
    
    while (!currentDate.isAfter(endDay)) {

      if(isWeekend(currentDate) && !dto.isIncludeWeekend()) {
        
        // Passa al giorno successivo
        currentDate = currentDate.plus(1, ChronoUnit.DAYS);
        continue;
      }

      var report = findReportOrCreateNew(currentDate.getYear(), currentDate.getMonthValue(), dipendente);

      if(StatoReport.RIFIUTATO.equals(report.getStato())) report.statoReport(StatoReport.IN_LAVORAZIONE);
            
      Commessa commessa = commessaRepository.findById(dto.getCommessaId()).orElseThrow(() -> new NotFoundException("Commessa con id " + dto.getCommessaId() + " non trovata"));
      
      if(dto.getStart().toLocalTime().isBefore(startBreak) && dto.getEnd().toLocalTime().isAfter(endBreak)) {
        var turnoMattina = new Turno()
            .start(LocalDateTime.of(currentDate, starTime))
            .end(LocalDateTime.of(currentDate, startBreak))
            .title(dto.getTitle())
            .commessa(commessa)
            .numProtocollo(dto.getNumeroProtocollo())
            .dipendente(dipendente);
        
        var turnoPomeriggio = new Turno()
            .start(LocalDateTime.of(currentDate, endBreak))
            .end(LocalDateTime.of(currentDate, endTime))
            .title(dto.getTitle())
            .commessa(commessa)
            .numProtocollo(dto.getNumeroProtocollo())
            .dipendente(dipendente);
        
        if(turnoMattina.getStart().equals(turnoMattina.getEnd())
            || turnoPomeriggio.getStart().equals(turnoPomeriggio.getEnd())) throw new BadRequestException("Il turno dev'essere almeno di un'ora");
                        
          // Aggiungo il Turno al Report
          report.addTurno(turnoMattina);
          report.addTurno(turnoPomeriggio);
  
          reportMensileRepository.save(report);
          turnoMattina.setReport(report);
          turnoPomeriggio.setReport(report);
  
          turnoRepository.save(turnoMattina); 
          turnoRepository.save(turnoPomeriggio); 
        
      }
      else {
        
        var turno = new Turno()
            .start(LocalDateTime.of(currentDate, starTime))
            .end(LocalDateTime.of(currentDate, endTime))
            .title(dto.getTitle())
            .commessa(commessa)
            .numProtocollo(dto.getNumeroProtocollo())
            .dipendente(dipendente);
        if(turno.getStart().equals(turno.getEnd())) throw new BadRequestException("Il turno dev'essere almeno di un'ora");

        if(isDuringLunch(turno)) throw new BadRequestException("Il turno non può essere durante la pausa pranzo");      

        // Aggiungo il Turno al Report
        report.addTurno(turno);
        reportMensileRepository.save(report);
        turno.setReport(report);
        turnoRepository.save(turno); 
        
      }
      
      // Passa al giorno successivo
      currentDate = currentDate.plus(1, ChronoUnit.DAYS);
    }
  }

  @Transactional
  public void update(UpdateTurnoDTO dto) {

    Turno turno = turnoRepository.findById(dto.getId())
        .orElseThrow(() -> new NotFoundException("Turno con id " + dto.getId() + " non trovato"));
    ReportMensile reportDelTurno = turno.getReport();
    reportDelTurno.removeTurno(turno);  
    turnoRepository.deleteById(dto.getId());
    save(dto);
  }
  
  @Transactional
  public void delete(Long id) {
    Turno turno = turnoRepository.findById(id)
        .orElseThrow(() -> new NotFoundException("Turno con id " + id + " non trovato"));
    
    ReportMensile report = turno.getReport();
    
    if(StatoReport.RIFIUTATO.equals(report.getStato())) {
      report.statoReport(StatoReport.IN_LAVORAZIONE);
      reportMensileRepository.save(report);
    }    
    
    turnoRepository.deleteById(id);
  }

  private boolean isWeekend(LocalDate date) {
    DayOfWeek dayOfWeek = date.getDayOfWeek();
    return dayOfWeek == DayOfWeek.SATURDAY || dayOfWeek == DayOfWeek.SUNDAY;
  }
  
  private boolean overlaps(CreateTurnoDTO turnoDaInserire, Dipendente dipendente){
    TurnoCriteria criteria = new TurnoCriteria();
    criteria.setStart(turnoDaInserire.getStart().toLocalDate().atStartOfDay());
    criteria.setEnd(turnoDaInserire.getEnd().toLocalDate().atTime(23, 59));
    criteria.setDipendente(dipendente.getCodiceFiscale());
    
    List<Turno> turniTrovati = this.findAll(criteria);
    
    
    for(Turno turnoPresente : turniTrovati) {
      
      LocalTime inizioTurnoPresente = turnoPresente.getStart().toLocalTime();
      LocalTime fineTurnoPresente = turnoPresente.getEnd().toLocalTime();
      
      LocalTime inizioTurnoDaInserire = turnoDaInserire.getStart().toLocalTime();
      LocalTime fineTurnoDaInserire = turnoDaInserire.getEnd().toLocalTime();

      if(!(inizioTurnoPresente.isAfter(fineTurnoDaInserire) || inizioTurnoPresente.equals(fineTurnoDaInserire)
          || fineTurnoPresente.isBefore(inizioTurnoDaInserire) || fineTurnoPresente.equals(inizioTurnoDaInserire))) {

            return true;
      }
    }
    
    return false;
  }
  
  private boolean isDuringLunch(Turno turno) {
    LocalTime inizioPausa = LocalTime.of(13,0);
    LocalTime finePausa = LocalTime.of(14,0);
    
    LocalTime inizioTurnoDaInserire = turno.getStart().toLocalTime();
    LocalTime fineTurnoDaInserire = turno.getEnd().toLocalTime();
    
    return (inizioTurnoDaInserire.isAfter(inizioPausa) && inizioTurnoDaInserire.isBefore(finePausa))
        || (fineTurnoDaInserire.isAfter(inizioPausa) && fineTurnoDaInserire.isBefore(finePausa))
        || (inizioTurnoDaInserire.equals(inizioPausa) || fineTurnoDaInserire.equals(finePausa));  
  }

  private ReportMensile findReportOrCreateNew(Integer anno, Integer mese, Dipendente dipendente) {
    return reportMensileRepository
        .findByAnnoAndMeseAndDipendente(anno, mese, dipendente)
        .orElse(new ReportMensile()
            .anno(anno)
            .mese(mese)
            .dipendente(dipendente)
            );
  }

  private Specification<Turno> createSpecification(TurnoCriteria criteria) {
    Specification<Turno> specification = Specification.where(null);
    if (criteria != null) {

      if (criteria.getStart() != null) {
        
        specification = specification
            .and((root, query, cb) -> cb.greaterThanOrEqualTo(root.get(Turno_.start), criteria.getStart()));
      }
      if (criteria.getEnd() != null) {
        specification = specification
            .and((root, query, cb) -> cb.lessThanOrEqualTo(root.get(Turno_.end), criteria.getEnd()));
      }
      
      if (!SecurityUtils.isCurrentUserInRole(Constants.RESPONSABILE)) {
        
        // Prelevo l'Utente
        String currentUserName = SecurityUtils.getCurrentUserLogin().get();  
        
        StringFilter user = new StringFilter();
        user.setEquals(currentUserName);
        
        specification = specification.and(buildSpecification(user, 
            root -> root.join(Turno_.dipendente, JoinType.LEFT).get(Dipendente_.codiceFiscale)));
      }

     
    }
    
    return specification;
  }
}