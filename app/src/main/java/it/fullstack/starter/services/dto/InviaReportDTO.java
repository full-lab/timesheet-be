package it.fullstack.starter.services.dto;
import lombok.Data;

@Data
public class InviaReportDTO {
  
  private String nota;
  
}