package it.fullstack.starter.services.dto;

import it.fullstack.starter.domain.enumeration.StatoReport;
import lombok.Data;

@Data
public class ReportMensileCriteria {
  
  private Integer anno;
  
  private Integer mese;
   
  private String q;
  
  private StatoReport stato;
  
}