package it.fullstack.starter.services.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;

import lombok.Data;

@Data
public class TurnoCriteria {
  
  private LocalDateTime start;

  private LocalDateTime end;
  
  private String dipendente;

}
