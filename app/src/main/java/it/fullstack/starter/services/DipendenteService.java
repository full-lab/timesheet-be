package it.fullstack.starter.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fullstack.sdk.core.exception.domain.BadRequestException;
import it.fullstack.sdk.core.exception.domain.NotFoundException;
import it.fullstack.sdk.core.service.QueryService;
import it.fullstack.starter.domain.Dipendente;
import it.fullstack.starter.domain.Dipendente_;
import it.fullstack.starter.repository.DipendenteRepository;
import it.fullstack.starter.services.dto.DipendenteCriteria;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@AllArgsConstructor
public class DipendenteService extends QueryService<Dipendente>{

    private final DipendenteRepository repository;

    @Transactional(readOnly = true)
    public Optional<Dipendente> findOne(String id) {
      log.debug("Request to get Dipendente:{}", id);
      
        return repository.findById(id);
    }

    @Transactional(readOnly = true)
    public Page<Dipendente> findAll(DipendenteCriteria criteria,Pageable page) {
      log.debug("Request to get all Dipendente:{}");
      
        return repository.findAll(createSpecification(criteria),page);
    }

    @Transactional
    public Dipendente create(Dipendente dto) {
        return repository.save(dto);
    }

    @Transactional
    public Dipendente update(Dipendente dto) {
        if (dto.getCodiceFiscale() == null) {
            // Gestire la logica di validazione se necessario
            throw new BadRequestException("Id cannot be null");
        }

        if (!repository.existsById(dto.getCodiceFiscale())) {
            throw new NotFoundException("%s non esistente: %s", dto.getNome(), dto.getCodiceFiscale());
        }

        if (repository.existsByCodiceFiscaleAndDataChiusuraIsNotNull(dto.getCodiceFiscale())) {
            throw new BadRequestException("%s chiuso non può essere modificato: %s", dto.getNome(), dto.getCodiceFiscale());
        }

        return repository.save(dto);
    }

    @Transactional
    public void delete(String id) {
        repository.findById(id)
                .map(d -> (Dipendente) d.close())
                .map(repository::save)
                .orElseThrow(() -> new NotFoundException("%s non esistente: %s", id));
    }
    
    private Specification<Dipendente> createSpecification(DipendenteCriteria criteria) {
      Specification<Dipendente> specification = Specification.where(null);

      if (criteria != null) {
        if(criteria.getQ() != null && !criteria.getQ().isBlank()) {

          Specification<Dipendente> nome = startsUpperSpecification(root -> 
                                                      root
                                                          .get(Dipendente_.nome), criteria.getQ());

          Specification<Dipendente> cognome = startsUpperSpecification(root -> 
                                                      root
                                                          .get(Dipendente_.cognome), criteria.getQ());

          specification = specification.and(nome.or(cognome));
        }
      }

      return specification;
  }

}
