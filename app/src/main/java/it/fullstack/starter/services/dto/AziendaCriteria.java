package it.fullstack.starter.services.dto;

import lombok.Data;

@Data
public class AziendaCriteria {

  private String nome;
}
