package it.fullstack.starter.services.dto;

import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class UpdateTurnoDTO extends CreateTurnoDTO {

  @NotNull
  private Long id;
}
