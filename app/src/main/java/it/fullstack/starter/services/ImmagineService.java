package it.fullstack.starter.services;

import java.io.IOException;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fullstack.sdk.core.exception.domain.BadRequestException;
import it.fullstack.starter.domain.Azienda;
import it.fullstack.starter.domain.Immagine;
import it.fullstack.starter.repository.AziendaRepository;
import it.fullstack.starter.repository.ImmagineRepository;
import it.fullstack.starter.services.dto.UploadImmagineDTO;
import it.fullstack.starter.services.mapper.ImmagineMapper;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class ImmagineService {

  private final ImmagineRepository immagineRepository;
  private final AziendaRepository aziendaRepository;

  @Transactional(readOnly = true)
  public Optional<Immagine> findOne(Long id) {
    
    return  immagineRepository.findById(id);
  }
  
  @Transactional
  public Immagine uploadImage(UploadImmagineDTO file) throws IOException {
    if (file.getImmagine().isEmpty()) throw new BadRequestException("Nessuna immagine caricata");
    
    Immagine immagine = ImmagineMapper.toEntity(file);
    
    Azienda aziendaImg = aziendaRepository
        .findById(file.getAziendaId())
        .orElseThrow(() -> new BadRequestException("L'id inserito non corrisponde a nessuna azienda"));
      
    if(aziendaImg.getLogo() != null) throw new BadRequestException("Non puoi sovrascrivere l'immagine, devi prima eliminarla.");
      
    aziendaImg.setLogo(immagine);

    immagineRepository.save(immagine);
    
    return immagine;
  }
  
  @Transactional
  public void deleteById(Long id) {
    immagineRepository.deleteById(id);
  }
}