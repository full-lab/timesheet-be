package it.fullstack.starter.services.mapper;

import java.io.IOException;

import it.fullstack.starter.domain.Immagine;
import it.fullstack.starter.services.dto.UploadImmagineDTO;

public interface ImmagineMapper {

  public static Immagine toEntity(UploadImmagineDTO dto) throws IOException {
    Immagine immagine = new Immagine();
    immagine.setNomeImmagine(dto.getImmagine().getOriginalFilename());
    immagine.setImgByte(dto.getImmagine().getBytes());
    immagine.setDimensioni(dto.getImmagine().getSize());

    return immagine;
  }
}