package it.fullstack.starter.config;

import static java.util.Optional.of;

import java.util.Optional;

import org.springframework.boot.actuate.endpoint.SecurityContext;

/**
 * MOCK Utility class for Spring Security.
 */
public class SecurityUtils {

  protected SecurityUtils() {}

  /**
   * Get the login of the current user.
   *
   * @return the login of the current user.
   */
  public static Optional<String> getCurrentUserLogin() {
    return of("MOCK");
  }
  
  public static boolean isCurrentUserInRole(String authority) {

    return false;
  }
}
