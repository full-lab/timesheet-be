package it.fullstack.starter.config;

/**
 * Constants for Spring Security authorities.
 */
public final class Constants {

  public static final String SYSTEM_ACCOUNT = "system";

  // Ruoli //
  public final static String RESPONSABILE = "ROLE_CNT_RESPONSABILE";
  public final static String OPERATORE_DG = "ROLE_CNT_OPERATORE_DG";
  public final static String OPERATORE_DR = "ROLE_CNT_OPERATORE_DR";

  private Constants() {
  }

}
