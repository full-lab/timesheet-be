package it.fullstack.starter.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.HeadersConfigurer.FrameOptionsConfig;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.SecurityFilterChain;

@Profile("mock-security")
@Configuration
@EnableWebSecurity
public class WebSecurityConfig {

  @Value("${users.user1.name}")
  private String user1Name;

  @Value("${users.user1.password}")
  private String user1Password;

  @Value("${users.user1.role}")
  private String user1Role;

  @Value("${users.user2.name}")
  private String user2Name;

  @Value("${users.user2.password}")
  private String user2Password;

  @Value("${users.user2.role}")
  private String user2Role;

  private final Environment environment;

  public WebSecurityConfig(Environment environment) {
      this.environment = environment;
  }

  @Bean
  public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
      List<String> activeProfiles = new ArrayList<>(Arrays.asList(environment.getActiveProfiles()));
      String profileActive = "default";
      
      if (activeProfiles.contains("no-login")) {
        profileActive = "no-login";
    } else if (activeProfiles.contains("login")) {
        profileActive = "login";
    }

      if (profileActive.equals("no-login")) {
        return configureNoLogin(http);
      }
      
    return configureLogin(http);
}

  private DefaultSecurityFilterChain configureLogin(HttpSecurity http) throws Exception {
    http
    .csrf(csrf -> csrf.disable())
    .authorizeHttpRequests((requests) -> requests
        .requestMatchers("/h2-console/**").permitAll()
        .requestMatchers("/", "/home").permitAll()
        .anyRequest().authenticated())
    .headers(headers -> headers.frameOptions(FrameOptionsConfig::disable))
    .formLogin((form) -> form.permitAll())
    .logout((logout) -> logout.permitAll());
    
    return http.build();
  }

  private DefaultSecurityFilterChain configureNoLogin(HttpSecurity http) throws Exception {
    http
    .csrf(csrf -> csrf.disable())
    .authorizeHttpRequests((requests) -> requests
        .requestMatchers("/**").permitAll()
        .requestMatchers("/h2-console/**").permitAll()
        .requestMatchers("/", "/home").permitAll()
        .anyRequest().authenticated()
        )
    .headers(headers -> headers.frameOptions(FrameOptionsConfig::disable))
    .formLogin((form) -> form.disable())
    .logout((logout) -> logout.permitAll());
    
    return http.build();
  }
 
  @Bean
  public UserDetailsService userDetailsService() {
    UserDetails user1 = createUser(user1Name, user1Password, user1Role);
    UserDetails user2 = createUser(user2Name, user2Password, user1Role);

    return new InMemoryUserDetailsManager(user1, user2);
  }

  private UserDetails createUser(String username, String password, String role) {
    return User.withDefaultPasswordEncoder()    //FIXME
        .username(username)                     //deprecato perchè non sicuro in produzione
        .password(password)                     //va bene in ambienti di testing e dev
        .roles(role) //("USER","ADMIN");
        .build();
  }

} 