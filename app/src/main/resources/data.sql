
-- Aggiorna la tabella AZIENDA
INSERT INTO AZIENDA (NOME, PARTITA_IVA, CREATED_BY, CREATED_DATE)
VALUES 
('Sogei', '01043931003', 'SYSTEM', NOW()),
('NTT Data', '07988320011', 'SYSTEM', NOW()),
('Reply', '05944141000', 'SYSTEM', NOW()),
('Fullstack', '16641971003', 'SYSTEM', NOW());

-- Aggiorna la tabella COMMESSA
INSERT INTO COMMESSA (NOME, TIPO_COMMESSA, AZIENDA_ID, CREATED_BY, CREATED_DATE, COLOR)
VALUES 
('MALATTIA', 'AMMINISTRATIVO', 1, 'SYSTEM', NOW(), 'bg-pink-500'), -- Orange
('FERIE', 'AMMINISTRATIVO', 1, 'SYSTEM', NOW(), 'bg-teal-500'),  -- Blue
('PERMESSO', 'AMMINISTRATIVO', 1, 'SYSTEM', NOW(), 'bg-green-500'), -- Green
('ProjectFullstack1', 'TASK', 2, 'SYSTEM', NOW(), 'bg-red-500'), -- Yellow
('ProjectFullstack2', 'TASK', 2, 'SYSTEM', NOW(), 'bg-red-400'), -- Yellow
('ProjectFullstack3', 'TASK', 2, 'SYSTEM', NOW(), 'bg-red-300'); -- Yellow

-- Aggiorna la tabella MANSIONE
INSERT INTO MANSIONE (DESCRIZIONE, CREATED_BY, CREATED_DATE)
VALUES
('JUNIOR', 'SYSTEM', NOW()),
('SENIOR', 'SYSTEM', NOW()),
('DATA_ANALYST', 'SYSTEM', NOW());

-- Aggiorna la tabella DIPENDENTE
INSERT INTO DIPENDENTE (CODICE_FISCALE, DATA_NASCITA, COGNOME, EMAIL, NOME, MANSIONE_ID, CREATED_BY, CREATED_DATE)
VALUES 
('MOCK', '1988-11-10', 'Bianchi', 'bianchi@example.com', 'Laura Giulia', 1, 'SYSTEM', NOW()),
('MNOP901234', '1992-07-03', 'Bianchi', 'neri@example.com', 'Laura Maria', 1, 'SYSTEM', NOW()),
('QRST567890', '1983-03-25', 'Gialli Ferrara', 'gialli@example.com', 'Marco', 2, 'SYSTEM', NOW()),
('QRST567891', '1983-03-25', 'Bianchi', 'gialli@example.com', 'Kiko', 2, 'SYSTEM', NOW()),
('UVWX678901', '1995-09-08', 'Rosa', 'rosa@example.com', 'Giulia', 3, 'SYSTEM', NOW());

-- Aggiorna la tabella DIPENDENTE_COMMESSE
INSERT INTO DIPENDENTE_COMMESSE(COMMESSE_ID, DIPENDENTE_CODICE_FISCALE) VALUES
(1, 'MOCK'),
(2, 'MOCK'),
(3, 'MOCK'),
(4, 'MOCK'),
(2, 'QRST567890'),
(4, 'QRST567890'),
(4, 'QRST567890'),
(2, 'UVWX678901');
INSERT INTO report_mensile (ANNO, MESE, DIPENDENTE_CODICE_FISCALE, STATO, NOTA_INVIO, NOTA_RIFIUTO)
VALUES 
    (2023, 12, 'MNOP901234', 'APPROVATO', 'Nota 1', null),
    (2023, 10, 'MOCK', 'RIFIUTATO', 'Nota 2', null),
    (2023, 08, 'QRST567890', 'IN_LAVORAZIONE', 'Nota 3', null),
    (2023, 11, 'UVWX678901', 'RICHIESTA_VALIDAZIONE', 'Nota 4', null),
    (2023, 07, 'MOCK', 'RIFIUTATO', 'Nota 5', null),
    (2023, 09, 'MNOP901234', 'RIFIUTATO', 'Nota 6', null),
    (2023, 06, 'MOCK', 'RICHIESTA_VALIDAZIONE', 'Nota 7', null),
    (2023, 05, 'QRST567890', 'RICHIESTA_VALIDAZIONE', 'Nota 8', null),
    (2023, 04, 'MOCK', 'RICHIESTA_VALIDAZIONE', 'Nota 9', null),
    (2023, 03, 'UVWX678901', 'RICHIESTA_VALIDAZIONE', 'Nota 10', null),
    (2023, 03, 'QRST567891', 'RICHIESTA_VALIDAZIONE', 'Nota 10', null),
    (2050, 13, 'MOCK', 'RICHIESTA_VALIDAZIONE', 'Nota 11', null);

    
