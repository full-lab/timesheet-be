# STARTER

## Kubernetes deployment Variables
- Env
  - PROFILES_ACTIVE: [val|prod|sviluppo]

- Kafka
  - KAFKA_USERNAME
  - KAFKA_PASSWORD

- Mysql
  - DB_MYSQL_HOSTNAME
  - DB_MYSQL_PORT
  - DB_MYSQL_DATABASE
  - DB_MYSQL_USERNAME
  - DB_MYSQL_PASSWORD

- MongoDB
  - DB_MONGODB_USERNAME
  - DB_MONGODB_PASSWORD
  - DB_MONGODB_HOSTNAME
  - DB_MONGODB_PORT
  - DB_MONGODB_DATABASE  

### Ruoli e utenze

Il servizio necessita dei seguenti ruoli:

- `CNT_OPERATORE_DG`

## How to Run
- Maven
  - Linux: `./mvnw spring-boot:run`
  - Windows: `./mvnw.cmd spring-boot:run`

## Validazione
- https: (Api Url)

## Produzione
- https: (Api Url)  


# Configurazione
## Swagger-ui configuration

Da file application.yml modificare le seguenti labels della sezione **openapi**:

```yaml

openapi:
	enabled: true
   title: <Titolo> 
   version: <Versione>
   description: <Descrizione>
   servers: #Array di servers
      - description: <Nome>
        url: <url>

```

Di default viene applicato il **lucchetto** per tutti gli endpoint esposti dall'applicazione.

Se un particolare controller o endpoint **non** deve risultare protetto, inserire la seguente annotation:

```java
import io.swagger.v3.oas.annotations.security.SecurityRequirements;
@SecurityRequirements(values = {})

```

a livello di endpoint singolo:

```java
@GetMapping
@SecurityRequirements(values = {})
public ResponseEntity<List<Elem>> findAll() {
    ...
}
```

o a livello di RestController:

```java
@RestController
@RequestMapping("/api/elem")
@SecurityRequirements(values = {})
public class ElemResource {
   ...
}
```


